var svgicons2svgfont = require('svgicons2svgfont')
var fs = require('fs')
var os = require('path')

const ARGS_RAW = process.argv.slice(2)
const BASE_PATH   = os.join(__dirname, '..')
const GLYPHS_PATH = os.join(BASE_PATH, 'assets', 'glyphs')
const ICONFONT_DATA = os.join(GLYPHS_PATH, 'index.json')
const ICONFONT_TARGET_PATH = os.join(BASE_PATH, 'assets')

// Beautify input args
let ARGS = ARGS_RAW.map(arg => arg.replace(/^([\-\s]+)/, ''))

// Read the glyphs
fs.readFile(ICONFONT_DATA, (err, data) => err ? showErrorMessage(err) :readGlyphs(data))

function readGlyphs (data) {
  data = JSON.parse(data)

  let glyphs    = {}

  for (let key in data.glyphs) {
    let glyph = data.glyphs[key]
    let { name, char, variants } = glyph
    let metadata = {
      unicode: [char],
      name: name.replace(/(\s+)/g, '-'),
    }
    for (let style in variants) {
      if (!glyphs[style])
        glyphs[style] = []
      let file = os.resolve(GLYPHS_PATH + '/' + variants[style])
      console.info(file, metadata)
      glyphs[style].push({ file, metadata })
    }
  }

  let fontInfo  = {
    fontName: data.title.replace(/(\s+)/g, '-'),
  }
  console.log(glyphs)
  for (let variant in glyphs) {
    console.info("Writing font " + variant + "...")
    let info  = Object.assign({}, fontInfo, {
      fontName: data.title,
    })
    // writeFont(glyphs[variant], fontInfo, variant)
  }

  let subset = 'regular'
  let variant = ARGS.indexOf('variant')
  if (variant !== -1)
    subset = ARGS[variant + 1]

  writeFont(glyphs[subset], fontInfo)

  console.log("Subset " + subset)
  console.log(ARGS)
}

let stream

function writeFont (subset, fontInfo, variant='regular') {
  stream = svgicons2svgfont({ fontName: fontInfo.fontName + '-' + variant })
  return new Promise(function(resolve, reject) {
    let glyphs = [...subset]
    let file = fontInfo.fontName + '-' + variant + '.svg'
    let path = os.join(ICONFONT_TARGET_PATH, file)
    let write = fs.createWriteStream(path)
    stream.pipe(write)
      .on('finish', () => {
        resolve({glyphs, path})
        write.end()
      })
      .on('error', (msg) => reject({error: msg}))

    for (let glyph of glyphs) {
      console.info("Writing glyph", glyph)
      try {
        let r = fs.createReadStream(glyph.file)
        r.metadata = glyph.metadata
        stream.write(r)
      }
      catch (err) {
        showErrorMessage(err)
      }
      // writeGlyph(stream, glyph)
    }

    stream.end()
  })
}

function writeGlyph (stream, { file, metadata }) {
  console.info("Creating read stream for", file)
  try {
    let glyph = fs.createReadStream(file)
    glyph.metadata = metadata
    stream.write(glyph)
  }
  catch (err) {
    showErrorMessage(err)
  }
}

function showErrorMessage (err) {
  console.warn(err)
  return null
}

// // Setting the font destination
// fontStream.pipe(fs.createWriteStream('fonts/hello.svg'))
//   .on('finish',function() {
//     console.log('Font successfully created!')
//   })
//   .on('error',function(err) {
//     console.log(err);
//   });
//
// // Writing glyphs
// var glyph1 = fs.createReadStream('icons/icon1.svg');
// glyph1.metadata = {
//   unicode: ['\uE001\uE002'],
//   name: 'icon1'
// };
// fontStream.write(glyph);
// // Multiple unicode values are possible
// var glyph2 = fs.createReadStream('icons/icon1.svg');
// glyph2.metadata = {
//   unicode: ['\uE002', '\uEA02'],
//   name: 'icon2'
// };
// fontStream.write(glyph2);
// // Either ligatures are available
// var glyph3 = fs.createReadStream('icons/icon1.svg');
// glyph3.metadata = {
//   unicode: ['\uE001\uE002'],
//   name: 'icon1-icon2'
// };
// fontStream.write(glyph3);

// Do not forget to end the stream
// fontStream.end();
